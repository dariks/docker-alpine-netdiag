FROM alpine:3.11

ENV DEBIAN_FRONTEND noninteractive

RUN echo "@edge-testing http://dl-cdn.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories
RUN echo "alias l='ls -lah'" >> /etc/profile

RUN apk add tzdata && \
    cp /usr/share/zoneinfo/Europe/Berlin /etc/localtime && \
    echo "Europe/Berlin" > /etc/timezone && \
    rm -rf /var/cache/apk/*

RUN apk update && \
    apk add bash \
            sudo \
            htop \
            curl \
            nano \
            wget \
            rsync \
            screen \
            openssh-client \
            ca-certificates \
            iproute2 \
            bind-tools \
            busybox-extras && \
    rm -rf /var/cache/apk/*

CMD ["/bin/sleep","infinity"]
